<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>
<head>
    <title>Login</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="util.css">
    <link rel="stylesheet" type="text/css" href="main.css">
    <link rel="stylesheet" type="text/css" href="css/style1.css" />
    <style>
      .error {
        border: 2px solid red;
      }
    </style>
  </head>
  
<body style="background-color: #666666;">
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <form class="login100-form validate-form" action="" method="post">
            <?php 
                if (!empty($_COOKIE['error'])) {
                setcookie('error', '', 100000);
                print('<div id="error"><strong>неверный логин или пароль!</strong></div>');
                }
            ?>
          <div class="wrap-input100 validate-input">
            <input class="input100 <?php if (!empty($_COOKIE['error'])) {
                                    setcookie('error', '', 100000);
                                    print 'error';
                                    }?>" 
                                    type="text" placeholder="Login" name="login" value="">
            <span class="focus-input100"></span>
            <span class="label-input100"></span>
          </div>

          <div class="wrap-input100 validate-input">
               <input class="input100 <?php if (!empty($_COOKIE['error'])) {
                                            setcookie('error', '', 100000);
                                            print 'error';
                                        }
                                    ?>" 
                    type="password"  maxlength="8" placeholder="password" name="pass" value="">
                <span class="focus-input100"></span>
                <span class="label-input100"></span>
          </div>
          
          <div class="container-login100-form-btn">
             <input class="login100-form-btn" type="submit" value="Войти">
          </div>
          <br>
          <div class="container-login100-form-btn">
             <input class="login100-form-btn" type="button" onclick="window.location= './';" value="Назад">
          </div>
        </form>
        <div class="login100-more" style="background-image: url('bg-01.jpg');">
        </div>
      </div>
    </div>
  </div>

</body>


<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
    $user = 'u16354';
    $pass = '8478228';
    $db = new PDO(
      'mysql:host=localhost;dbname=u16354',
      $user,
      $pass,
      array(PDO::ATTR_PERSISTENT => true)
    );
    $_SESSION['login'] = '~';
    $stmt = $db->prepare('SELECT id, login FROM application5 WHERE login = ? AND password = ?');
    $stmt->execute([$_POST['login'], md5($_POST['pass'])]);
    while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
      $_SESSION['login'] = strip_tags($row['login']);
      $_SESSION['uid'] = $row['id'];
      header('Location: ./');
    }
  
    if ($_SESSION['login'] == '~') {
      setcookie('error', '1',  time() + 24 * 60 * 60);
      session_destroy();
      header('Location: login.php');
    }
  
 
}



